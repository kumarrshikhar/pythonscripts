import argparse
from datetime import datetime, timezone
from kubernetes import client, config

def cleanup_user_deployments(before):
    v1 = client.CoreV1Api()
    print("Deleting deployment namespaces that are older than %s" % (before))
    ret = v1.list_namespace()
    for i in ret.items:
        time_difference = (datetime.now(timezone.utc) - i.metadata.creation_timestamp).days
        if i.metadata.name.startswith('dep-ns') and int(time_difference) > before:
            print("Deleting deployment namespaces: %s" % (i.metadata.name))
            v1.delete_namespace(i.metadata.name)

def list_user_deployments(before):
    print("Listing user deployment namespaces that will be deleted: ")
    v1 = client.CoreV1Api()
    ret = v1.list_namespace()
    for i in ret.items:
        time_difference = (datetime.now(timezone.utc) - i.metadata.creation_timestamp).days
        if i.metadata.name.startswith('dep-ns') and int(time_difference) > before:
            print('%s\t%s\t%s' % (i.metadata.name, i.metadata.creation_timestamp, time_difference))


if __name__ == '__main__':
    config.load_incluster_config()
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--list-only', type=bool, nargs='?', required=True, help='List only deployments')
    parser.add_argument('--before', type=int, nargs='?', required=True, help='Delete all pods before X number of days')

    args = parser.parse_args()

    if args.list_only:
        list_user_deployments(args.before)
    else:
        list_user_deployments(args.before)
        cleanup_user_deployments(args.before)